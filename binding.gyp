{
  "targets": [
    {
      "target_name": "electron-test",
      "sources": [
        "binding.cpp",
        "ElectronTest.h",
        "ElectronTest.cpp"
      ],
      "include_dirs": [
        "<!(node -e \"require('nan')\")"
      ],
      "defines": [
        "DEBUG"
      ],
      "conditions": [ [
        "OS==\"linux\"",
        {
          "cflags_cc!": [
            "-fno-exceptions",
            "-fno-rtti"
          ],
          "cflags": [
            "-g",
            "-std=c++11"
          ]
        }
      ], [
        "OS==\"mac\"",
        {
          "xcode_settings": {
            "GCC_ENABLE_CPP_EXCEPTIONS": "YES",
            "GCC_ENABLE_CPP_RTTI": "YES",
            "CLANG_CXX_LANGUAGE_STANDARD": "c++0x",
          },
          "configurations": {
            "Debug": {
              "xcode_settings": {
                "INSTALL_DIR": [
                  "$PROJECT_DIR/Debug"
                ],
                "CONFIGURATION_BUILD_DIR": "$PROJECT_DIR/build/Debug"
              }
            },
            "Release": {
              "xcode_settings": {
                "INSTALL_DIR": [
                  "$PROJECT_DIR/Release"
                ],
                "CONFIGURATION_BUILD_DIR": "$PROJECT_DIR/build/Release"
              }
            }
          }
        }
      ] ]
    }
  ]
}
