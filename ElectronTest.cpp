#include <nan.h>

#include "ElectronTest.h"

Nan::Persistent<v8::Function> ElectronTest::constructor;
Nan::Persistent<v8::FunctionTemplate> ElectronTest::_template;

void ElectronTest::Init(v8::Local<v8::Object> exports) {
    Nan::HandleScope scope;

    // Prepare constructor template
    v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
    tpl->SetClassName(Nan::New("ElectronTest").ToLocalChecked());
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    // Prototype
    Nan::SetAccessor(tpl->InstanceTemplate(), Nan::New("attributes").ToLocalChecked(), GetAttributes);

    constructor.Reset(tpl->GetFunction());
    exports->Set(Nan::New("ElectronTest").ToLocalChecked(), tpl->GetFunction());

    _template.Reset(Nan::Persistent<v8::FunctionTemplate>(tpl));
}

void ElectronTest::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    if (!info.IsConstructCall()) {
        // Invoked as plain function `MyObject(...)`, turn into construct call.
        const int argc = 2;
        v8::Local<v8::Value> argv[argc] = { info[0], info[1] };
        v8::Local<v8::Function> cons = Nan::New<v8::Function>(constructor);
        info.GetReturnValue().Set(cons->NewInstance(argc, argv));
        
        return;
    }

    ElectronTest* obj = new ElectronTest(info);
    obj->Wrap(info.This());
    info.GetReturnValue().Set(info.This());
}

v8::Local<v8::Object> ElectronTest::NewInstance() {
    Nan::EscapableHandleScope scope;

    const unsigned argc = 1;
    v8::Local<v8::Value> argv[argc] = { Nan::Null() };
    v8::Local<v8::Function>cons = Nan::New<v8::Function>(constructor);
    v8::Local<v8::Object>instance = cons->NewInstance(argc, argv);

    return scope.Escape(instance);
}

ElectronTest::ElectronTest(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    auto attributes = Nan::New<v8::Object>();

    attributes->Set(Nan::New("should_be_true").ToLocalChecked(), Nan::New<v8::Boolean>(true));
    attributes->Set(Nan::New("should_be_false").ToLocalChecked(), Nan::New<v8::Boolean>(false));

    _attributes.Reset(attributes);
}
    
ElectronTest::~ElectronTest() {
}

void ElectronTest::GetAttributes(v8::Local<v8::String> name, const Nan::PropertyCallbackInfo<v8::Value>& info) {
    ElectronTest* obj = ObjectWrap::Unwrap<ElectronTest>(info.Holder());

    info.GetReturnValue().Set(Nan::New(obj->_attributes));
}
