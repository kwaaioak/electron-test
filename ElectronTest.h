#ifndef __ElectronTest__
#define __ElectronTest__

#include <nan.h>

class ElectronTest : public Nan::ObjectWrap {
    public:
        static void Init(v8::Local<v8::Object> exports);
        static v8::Local<v8::Object> NewInstance();

    private:
        explicit ElectronTest(const Nan::FunctionCallbackInfo<v8::Value>& info);
        ~ElectronTest();

    private:
        static Nan::Persistent<v8::Function> constructor;
        static Nan::Persistent<v8::FunctionTemplate> _template;
        static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);
        static void GetAttributes(v8::Local<v8::String> name, const Nan::PropertyCallbackInfo<v8::Value>& info);

    private:
        Nan::Persistent<v8::Object> _attributes;
};

#endif
