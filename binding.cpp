#include <nan.h>

#include "ElectronTest.h"

void ModuleInit(v8::Local<v8::Object> module) {
    Nan::HandleScope scope;

    ElectronTest::Init(module);
}

NODE_MODULE(psyx, ModuleInit)
