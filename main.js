var electron = typeof(process.versions['electron']) !== 'undefined' ? require('electron') : null,
    electron_test = (process.env.NODE_ENV === 'production')
    ? require('electron-test')
    : require('bindings')({ bindings: 'electron-test', module_root: __dirname }),
    util = require('util');

var result = new electron_test.ElectronTest();

var attributes = result.attributes;
for(var a in attributes)
  console.log(a + ' = (' + typeof(attributes[a]) + ')' + attributes[a]);

if (electron) {
  const {app} = electron;

  app.on('ready', function() {
    app.quit();
  });
}
